package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;

import logic.Agencia;
import logic.Grafo;
import maps.Map;

public class Controlador implements MouseListener, ActionListener
{
	// vista
	private Map vista;
	private JMapViewer map;
	private JButton btnAgregarAgente;
	private JButton btnConectarAgentes;
	private JButton btnBorrarInfo;
	
	//sirven de logica en la vista. 
	private boolean activarAgregarAgentes;
	private boolean activarConectarAgentes;
	
	//variables de codigo de negocio
	private Agencia agencia;
	
	public Controlador(Agencia agencia, Map vista)
	{
		this.agencia = agencia;
		this.vista = vista;
		this.map = vista.getMap();
		this.activarAgregarAgentes = false;
		this.activarConectarAgentes = false;
		this.map.addMouseListener(this);
		
		this.btnAgregarAgente = vista.getAgregarAgente();
		this.btnConectarAgentes = vista.getConectarAgentes();
		this.btnBorrarInfo = vista.getBorrarInfo();
		
		this.btnAgregarAgente.addActionListener(this);
		this.btnConectarAgentes.addActionListener(this);
		this.btnBorrarInfo.addActionListener(this);
	}
	
	//Dibuja el agm una vez generado
	public void dibujarAristas()
	{
		for(int agenteI = 0; agenteI < agencia.cantidadAgentes()-1; agenteI++)
		{
			for(int agenteJ = agenteI+1; agenteJ < agencia.cantidadAgentes(); agenteJ++)
			{
				if(agencia.getRelaciones().existeArista(agenteI,agenteJ))
				{
					Coordinate coor1 = this.agencia.getAgente(agenteI).getUbicacion();
					Coordinate coor2 = this.agencia.getAgente(agenteJ).getUbicacion();
					this.vista.dibujarConexion(coor1,coor2);
				}
			}
		}
	}
	
	public void mouseClicked(MouseEvent e)
	{
		if(this.activarAgregarAgentes)
		{
			Coordinate coordenada = this.map.getPosition(e.getPoint());
			agencia.agregarAgente(coordenada);
			//System.out.println("click");
			this.vista.dibujarAgente(coordenada);
		}
	}

	public void mouseEntered(MouseEvent e){}
	
	public void mouseExited(MouseEvent e){}
	
	public void mousePressed(MouseEvent e){}
	
	public void mouseReleased(MouseEvent e){}
	
	public void actionPerformed(ActionEvent e)
	{
		if(this.btnAgregarAgente.equals(e.getSource()))
		{
			if(!this.activarConectarAgentes)
			{
				this.activarAgregarAgentes = !this.activarAgregarAgentes;
				actualizarColorBtns();
			}
		}
		if(this.btnConectarAgentes.equals(e.getSource()))
		{
			if(!this.agencia.estaVacia())
			{
				this.activarAgregarAgentes = false;
				this.activarConectarAgentes = true;
				this.agencia.relacionarAgentes();
				double mayorPeso = this.agencia.getRelaciones().getMayorArista();
				double menorPeso = this.agencia.getRelaciones().getMenorArista();
				double pesoTotal = this.agencia.getRelaciones().getPesoTotal();

				this.vista.setAristaMayorPeso(mayorPeso);
				this.vista.setPesoTotal(pesoTotal);
				this.vista.setAristaMenorPeso(menorPeso);

				this.dibujarAristas();
				actualizarColorAgentes();
				actualizarColorBtns();
			}
		}
		if(this.btnBorrarInfo.equals(e.getSource()))
		{
			this.activarConectarAgentes = false;
			this.activarAgregarAgentes = false;
			this.vista.borrarInformacionMapa();
			this.agencia.desarmarAgencia();
			actualizarColorBtns();
		}
	}

	private void actualizarColorAgentes() {
		Grafo agentes = agencia.getRelaciones();
		for(int i = 0; i < agencia.cantidadAgentes(); i++)
		{
			if(i == agentes.getMayorVerticeI() || i == agentes.getMayorVerticeJ())
			{
				if(i == agentes.getMenorVerticeI() || i == agentes.getMenorVerticeJ())
				{
					vista.pintarAgenteAristaMinMax(i);
				}
				else
				{
					vista.pintarAgenteAristaMayor(i);
				}
			}
			else if(i == agentes.getMenorVerticeI() || i == agentes.getMenorVerticeJ())
			{
				vista.pintarAgenteAristaMenor(i);
			}
		}
	}

	private void actualizarColorBtns() {
		vista.setColorBtnAgregarAgentes(this.activarAgregarAgentes);
		vista.setColorBtnConectarAgentes(this.activarConectarAgentes);
	}
}
