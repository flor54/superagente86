package maps;

import javax.swing.JFrame;
import javax.swing.JLabel;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapObjectImpl;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;
import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;

import java.awt.Color;


//import java.awt.event.MouseAdapter;
//import java.awt.event.MouseEvent;
import javax.swing.JButton;

public class Map  extends JFrame
{
	private JMapViewer map;
	private int DIMENSIONX;
	private int DIMENSIONY;
	private JButton agregarAgente;
	private JButton conectarAgentes;
	private JButton borrarInfo;
	private JLabel pesoTotal;
	private JLabel aristaMayorPeso;
	private JLabel aristaMenorPeso;
	
	private static final long serialVersionUID = 1L;

	public Map()
	{
		super();
		this.DIMENSIONX = 900; 
		this.DIMENSIONY = 600; 
		this.setBounds(100, 100, DIMENSIONX+70, DIMENSIONY);
		this.setVisible(true);

		initialize();
	}

	private void initialize()
	{
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		this.map = new JMapViewer();
		
		int limiteMap = this.DIMENSIONX- 250;
		map.setBounds(0,0,limiteMap ,this.DIMENSIONY);
		
		this.getContentPane().add(this.map);
		int dimensionBtnsY = 51;
		int dimensionBtnsX =  300;
		
		agregarAgente = new JButton("Agregar Agente");
		agregarAgente.setBounds(limiteMap,0, dimensionBtnsX, dimensionBtnsY);
		getContentPane().add(agregarAgente);
		
		conectarAgentes = new JButton("Conectar Agentes");
		conectarAgentes.setBounds(limiteMap, dimensionBtnsY, dimensionBtnsX, dimensionBtnsY);
		getContentPane().add(conectarAgentes);
		
		borrarInfo = new JButton("Borrar");
		borrarInfo.setBounds(limiteMap, 2*dimensionBtnsY, dimensionBtnsX, dimensionBtnsY);
		getContentPane().add(borrarInfo);
		
		this.agregarAgente.setBackground(Color.BLUE);
		this.agregarAgente.setForeground(Color.WHITE);
		
		this.conectarAgentes.setBackground(Color.BLUE);
		this.conectarAgentes.setForeground(Color.WHITE);
		
		this.borrarInfo.setBackground(Color.RED);
		this.borrarInfo.setForeground(Color.WHITE);
		
		this.pesoTotal = new JLabel("");
		this.pesoTotal.setBounds(limiteMap, 3*dimensionBtnsY, dimensionBtnsX, dimensionBtnsY);
		this.aristaMayorPeso = new JLabel("");
		this.aristaMayorPeso.setBounds(limiteMap, 4*dimensionBtnsY, dimensionBtnsX, dimensionBtnsY);
		this.aristaMenorPeso = new JLabel("");
		this.aristaMenorPeso.setBounds(limiteMap, 5*dimensionBtnsY, dimensionBtnsX, dimensionBtnsY);
		
		this.getContentPane().add(this.pesoTotal);
		this.getContentPane().add(this.aristaMayorPeso);
		this.getContentPane().add(this.aristaMenorPeso);
	}
	
	public JLabel getPesoTotal() {
		return pesoTotal;
	}

	public void setPesoTotal(double peso) {
		this.pesoTotal.setText("Peso total: : "+peso);
	}

	public JLabel getAristaMayorPeso() {
		return aristaMayorPeso;
	}

	public void setAristaMayorPeso(double peso ) {
		this.aristaMayorPeso.setText("Arista mayor peso: "+peso);
	}

	public JLabel getAristaMenorPeso() {
		return aristaMenorPeso;
	}

	public void setAristaMenorPeso(double peso) {
		this.aristaMenorPeso.setText("Arista menor peso: "+peso);
	}

	public JMapViewer getMap() 
	{
		return this.map;
	}
	
	public JButton getAgregarAgente()
	{
		return this.agregarAgente;
	}
	
	public JButton getConectarAgentes()
	{
		return this.conectarAgentes;
	}
	
	public JButton getBorrarInfo()
	{
		return this.borrarInfo;
	}
	
	public void dibujarConexion(Coordinate coor1,Coordinate coor2 ) 
	{
		MapPolygon arista = new MapPolygonImpl(coor1,coor2,coor1,coor2);
		map.addMapPolygon(arista);
	}
	
	public void dibujarAgente(Coordinate c) 
	{
		MapMarker marker = new MapMarkerDot(c);
		map.addMapMarker(marker);
	}

	public void pintarAgenteAristaMenor(int agente)
	{
		((MapObjectImpl) map.getMapMarkerList().get(agente)).setBackColor(Color.blue);
	}
	
	public void pintarAgenteAristaMayor(int agente)
	{
		((MapObjectImpl) map.getMapMarkerList().get(agente)).setBackColor(Color.red);
	}
	
	public void pintarAgenteAristaMinMax(int agente)
	{
		((MapObjectImpl) map.getMapMarkerList().get(agente)).setBackColor(Color.magenta);
	}
	
	public void borrarInformacionMapa()
	{
		this.map.removeAllMapMarkers();
		this.map.removeAllMapPolygons();
		this.setPesoTotal(0);
		this.setAristaMenorPeso(0);
		this.setAristaMayorPeso(0);
	}

	public void setColorBtnAgregarAgentes(boolean estado)
	{
		if(estado)
		{
			this.agregarAgente.setBackground(Color.YELLOW);
			this.agregarAgente.setForeground(Color.BLACK);
		}
		else 
		{
			this.agregarAgente.setBackground(Color.BLUE);
			this.agregarAgente.setForeground(Color.WHITE);
		}
		
	}
	
	public void setColorBtnConectarAgentes(boolean estado)
	{
		if(estado)
		{
			this.conectarAgentes.setBackground(Color.YELLOW);
			this.conectarAgentes.setForeground(Color.BLACK);
		}
		else 
		{
			this.conectarAgentes.setBackground(Color.BLUE);
			this.conectarAgentes.setForeground(Color.WHITE);
		}
	}
}
