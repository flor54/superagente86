package main;

import controller.Controlador;
import logic.Agencia;
import maps.Map;

public class Main
{
	public static void main(String[] args)
	{
		Map vista = new Map();
		Agencia agencia = new Agencia();
		@SuppressWarnings("unused")
		Controlador controlador = new Controlador(agencia,vista);
	}
}
