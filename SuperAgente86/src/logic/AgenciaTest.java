package logic;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.Before;
import org.junit.Test;
import org.openstreetmap.gui.jmapviewer.Coordinate;

public class AgenciaTest {

	Agencia agenciaVacia;
	Agencia agenciaLlena;
	Agencia agenciaMuyGrande;
	
	@Before
	public void inicializar()
	{
		agenciaVacia = new Agencia();
		agenciaLlena = new Agencia();
		for(int i = 0; i < 6; i++)
			agenciaLlena.agregarAgente(ubicacionRandom());
		agenciaMuyGrande = new Agencia();
		for(int i = 0; i < 150; i++)
			agenciaMuyGrande.agregarAgente(ubicacionRandom());
	}
	
	@Test(expected = RuntimeException.class)
	public void agregarAgenteYaExistenteTest()
	{
		Coordinate ubicacion = new Coordinate(13.000, 13.000);
		agenciaLlena.agregarAgente(ubicacion);
		agenciaLlena.agregarAgente(ubicacion);
	}
	
	@Test(expected = RuntimeException.class)
	public void buscarAgenteInexistenteTest()
	{
		agenciaLlena.getAgente("006");
	}
	
	@Test
	public void eliminarAgenteTest()
	{
		agenciaLlena.eliminarAgente("005");
		assertEquals(5, agenciaLlena.cantidadAgentes());
	}
	
	@Test(expected = RuntimeException.class)
	public void buscarAgenteEliminadoTest()
	{
		agenciaLlena.eliminarAgente("006");
		agenciaLlena.getAgente("006");
	}

	@Test
	public void cantidadAgentesTest()
	{
		assertEquals(6, agenciaLlena.cantidadAgentes());
	}
	
	@Test
	public void getRelacionesTest()
	{
		agenciaLlena.relacionarAgentes();
		assertNotNull(agenciaLlena.getRelaciones());
	}
	
	@Test
	public void getAgenteTest()
	{
		Agente agente = agenciaLlena.getAgente("005");
		assertNotNull(agente);
	}
	
	@Test
	public void desarmarAgenciaTest()
	{
		agenciaLlena.desarmarAgencia();
		assertTrue(agenciaLlena.estaVacia());
	}
	
	static private Coordinate ubicacionRandom()
	{
		Random doubleRandom = new Random();
		Random intRandom = new Random();
		double lon = (double) (intRandom.nextInt(360) + doubleRandom.nextDouble())-180;
		double lat = (double) (intRandom.nextInt(180) + doubleRandom.nextDouble())-90;
		return new Coordinate(lat, lon);
	}
}
