package logic;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class GrafoTest {

	Grafo grafoVacio;
	Grafo grafoLleno;
	double[] distancias;
	
	@Before
	public void inicializarTest()
	{
		grafoVacio = new Grafo(0);
		grafoLleno = new Grafo(10);
		distancias = new double[10];
		distancias[0] = 13.900;
		distancias[1] = 15.000;
		distancias[2] = 56.000;
		distancias[3] = 9.000;
		distancias[4] = 0.567;
		distancias[5] = 43.905;
		distancias[6] = 100.900;
		distancias[7] = 23.000;
		distancias[8] = 12.000;
		for(int i = 1; i < grafoLleno.vertices(); i++)
		{
			grafoLleno.agregarArista(0,i,distancias[i-1]);
		}
	}
	
	@Test
	public void verticesTest()
	{
		assertEquals(10, grafoLleno.vertices());
	}
	
	@Test
	public void agregarAristaTest()
	{
		grafoLleno.agregarArista(1,4,15.900);
		assertTrue(grafoLleno.existeArista(1,4));
	}
	
	@Test
	public void eliminarAristaTest()
	{
		grafoLleno.agregarArista(1,4,15.900);
		grafoLleno.eliminarArista(1,4);
		assertFalse(grafoLleno.existeArista(1,4));
	}
	
	@Test
	public void existeVerticeTrueTest() 
	{
		assertTrue(grafoLleno.existeVertice(5));
	}
	
	@Test
	public void existeVerticeNegativoTest() 
	{
		assertFalse(grafoLleno.existeVertice(-1));
	}
	
	@Test
	public void existeVerticeFueraDeRangoTest() 
	{
		assertFalse(grafoLleno.existeVertice(12));
	}
	
	@Test
	public void verticesIMinMaxIgualesTestTrue()
	{
		grafoLleno.agregarArista(1,6,0.200);
		grafoLleno.agregarArista(1,9,200.00);
		assertTrue(grafoLleno.verticesIMinMaxIguales());
	}
	
	@Test
	public void verticesIMinMaxIgualesTestFalse()
	{
		grafoLleno.agregarArista(1,6,0.200);
		grafoLleno.agregarArista(2,9,200.00);
		assertFalse(grafoLleno.verticesIMinMaxIguales());
	}
	
	@Test
	public void verticesJMinMaxIgualesTestTrue()
	{
		grafoLleno.agregarArista(1,6,0.200);
		grafoLleno.agregarArista(2,6,200.00);
		assertTrue(grafoLleno.verticesJMinMaxIguales());
	}
	
	@Test
	public void verticesJMinMaxIgualesTestFalse()
	{
		grafoLleno.agregarArista(1,6,0.200);
		grafoLleno.agregarArista(2,9,200.00);
		assertFalse(grafoLleno.verticesJMinMaxIguales());
	}
	
	@Test
	public void pesoTotalGrafo3VerticesTest()
	{
		Grafo grafoTres = new Grafo(3);
		grafoTres.agregarArista(1,0,this.distancias[0]);
		grafoTres.agregarArista(1,2,this.distancias[1]);
		int esperado = (int) (this.distancias[0] + this.distancias[1]);
		int pesoTotal = (int) grafoTres.getPesoTotal();
		assertEquals(esperado, pesoTotal);
	}
	
	@Test
	public void agmTest()
	{
		Grafo agm = grafoLleno.agm();
		assertNotNull(agm);
	}
}
