package logic;

import org.openstreetmap.gui.jmapviewer.Coordinate;

public class Agente
{
	private String nombreClave;
	private Coordinate ubicacion;
	
	//Construye agente vacio
	public Agente()
	{
		this.nombreClave = null;
		this.ubicacion = null;
	}
	
	//Construye agente con nombre y ubicacion recibidos
	public Agente(String nombreClave, Coordinate ubicacion)
	{
		this.nombreClave = nombreClave;
		this.ubicacion = ubicacion;
	}
	
	//Devuelve nombre clave de agente
	public String getNombreClave()
	{
		verificarAgenteNombrado();
		return this.nombreClave;
	}
	
	//Devuelve ubicacion en coordenadas
	public Coordinate getUbicacion()
	{
		verificarAgenteUbicado();
		return this.ubicacion;
	}

	private void verificarAgenteNombrado()
	{
		if(this.nombreClave.equals(null))
			throw new NullPointerException("El agente accedido no tiene nombre");
	}

	private void verificarAgenteUbicado()
	{
		if(this.ubicacion.equals(null))
			throw new NullPointerException("El agente accedido no tiene ubicacion");
	}
}
