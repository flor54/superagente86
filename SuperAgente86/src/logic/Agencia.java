package logic;

import java.util.ArrayList;
import org.openstreetmap.gui.jmapviewer.Coordinate;

public class Agencia
{
	private ArrayList<Agente> agentes;
	private Grafo relaciones;
	
	//Crea agencia vacia para ir llenandola
	public Agencia()
	{
		this.agentes = new ArrayList<Agente>();
	}
	
	//Agrega agente con ubicacion
	public void agregarAgente(Coordinate ubicacion)
	{
		verificarAgenteInexistente(ubicacion);
		Agente agente = new Agente(nombrar(), ubicacion);
		this.agentes.add(agente);
	}

	//Elimina agente por nombre
	public void eliminarAgente(String nombreClave)
	{
		verificarAgenteExistente(nombreClave);
		int agente = 0;
		boolean eliminado = false;
		while(agente < cantidadAgentes() && !eliminado)
		{
			if(nombreClave.equals(getAgente(agente).getNombreClave()))
			{
				eliminarAgente(agente);
				eliminado = true;
			}
			agente++;
		}
	}

	//Elimina agente por indice
	public void eliminarAgente(int indice)
	{
		verificarIndiceAgente(indice);
		this.agentes.remove(indice);
	}

	//Devuelve cantidad de agentes
	public int cantidadAgentes()
	{
		return this.agentes.size();
	}
	
	//Devuelve si la agencia esta vacia
	public boolean estaVacia()
	{
		return cantidadAgentes() == 0;
	}
	
	//Devuelve agente por nombre
	public Agente getAgente(String nombreClave)
	{
		verificarAgenteExistente(nombreClave);
		boolean encontrado = false;
		int agente = 0;
		Agente buscado = null;
		while(agente < cantidadAgentes() && !encontrado)
		{
			if(nombreClave.equals(getAgente(agente).getNombreClave()))
			{
				buscado = getAgente(agente);
				encontrado = true;
			}
			agente++;
		}
		return buscado;
	}
	
	//Devuelve agente por indice
	public Agente getAgente(int indice)
	{
		verificarIndiceAgente(indice);
		return this.agentes.get(indice);
	}
		
	//Calcula la distancia en km entre agentes desplegados
	private double distanciaEntreAgentes(int agente1, int agente2)
	{
		verificarIndiceAgente(agente1);
		verificarIndiceAgente(agente2);
		Agente agente01 = getAgente(agente1);
		Agente agente02 = getAgente(agente2);
		double lat1 =  agente01.getUbicacion().getLat();
		double lon1 =  agente01.getUbicacion().getLon();
		double lat2 =  agente02.getUbicacion().getLat();
		double lon2 =  agente02.getUbicacion().getLon();
		
		//TODO:deberia darse opcion de millas o kilometros?
		double RADIOTIERRA = 6371; 
		double dLat = Math.toRadians(lat2 - lat1);  
		double dLng = Math.toRadians(lon2 - lon1);  
		double sindLat = Math.sin(dLat / 2);  
		double sindLng = Math.sin(dLng / 2);  
		double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2) * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));  
		double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));  
		double distancia = RADIOTIERRA * va2;  
		
	    return distancia;
	}
	
	//Devuelve las relaciones entre agentes
	public Grafo getRelaciones()
	{
		return this.relaciones;
	}
	
	//Aplica relaciones entre los agentes una vez se termine
	//la contratacion de todos ellos y se crea su agm correspondiente
	public void relacionarAgentes()
	{
		//TODO:debe crear agm de grafo de todos los agentes
		this.relaciones = this.cliqueAgentes();
		//TODO:correcto?
		Grafo agm = this.relaciones.agm();
		this.relaciones = agm;
	}
	
	//Devuelve un grafo con todos sus elementos relacionados
	//y los pesos de sus respectivas aristas
	private Grafo cliqueAgentes()
	{
		Grafo clique = new Grafo(cantidadAgentes());
		double distancia;
		
		for(int agenteI = 0; agenteI < cantidadAgentes()-1; agenteI++)
		{
			for(int agenteJ = agenteI+1; agenteJ < cantidadAgentes(); agenteJ++)
			{
				distancia = distanciaEntreAgentes(agenteI, agenteJ);
				clique.agregarArista(agenteI, agenteJ, distancia);
			}
		}
		return clique;
	}
	
	//Elimina relaciones actuales de agentes
	private void desligarAgentes()
	{
		this.relaciones = null;
	}
	
	//Vacia la agencia actual y arma una nueva
	public void desarmarAgencia()
	{
		desligarAgentes();
		this.agentes = new ArrayList<Agente>();
	}

	//Da nombre por defecto a agente agregado
	public String nombrar()
	{
		String ret = "";
		if(cantidadAgentes() < 10)
		{
			ret = "00" + cantidadAgentes();
		}
		else if(cantidadAgentes() >= 100)
		{
			ret = ret + cantidadAgentes();
		}
		else
		{
			ret = "0" + cantidadAgentes();
		}
		return ret;
	}
	
	//Verifica si ya existe agente con tal nombre
	private void verificarAgenteExistente(String nombreClave)
	{
		int agente = 0;
		boolean existe = false;
		while(agente < cantidadAgentes() && !existe)
		{
			String nombreAgente = getAgente(agente).getNombreClave();
			existe = nombreClave.equals(nombreAgente);
			agente++;
		}
		if(!existe)
			throw new RuntimeException("El agente buscado no existe");
	}

	//Funcion para detectar si no existe ya un agente en una ubicacion
	private void verificarAgenteInexistente(Coordinate ubicacion)
	{
		int agente = 0;
		boolean existe = false;
		while(agente < cantidadAgentes() && !existe)
		{
			Coordinate ubicacionAgente = getAgente(agente).getUbicacion();
			existe = existe || ubicacion.equals(ubicacionAgente);
			agente++;
		}
		if(existe)
			throw new RuntimeException("Ya existe un agente desplegado en la ubicaci�n recibida");
	}

	//Verifica que el indice recibido sea valido
	private void verificarIndiceAgente(int indice)
	{
		if(indice < 0 || indice >= cantidadAgentes())
			throw new IllegalArgumentException("El valor recibido no es v�lido");
	}
}
