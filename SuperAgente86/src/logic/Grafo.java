package logic;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Grafo
{
	// Representamos un grafo por listas de vecinos
	private ArrayList<Set<Integer>> _vecinos;
	private double[][] pesoAristas;
	private double menorArista;
	private int menorVerticeI;
	private int menorVerticeJ;
	private double mayorArista;
	private int mayorVerticeI;
	private int mayorVerticeJ;
	private double pesoTotal;

	// La cantidad de v�rtices queda fija desde la construcci�n
	public Grafo(int vertices)
	{
		// El grafo comienza con todos los v�rtices aislados
		verificarCantidadVerticesValido(vertices);
		_vecinos = new ArrayList<Set<Integer>>(vertices);
		pesoAristas = new double[vertices][vertices];
		
		for(int i=0; i<vertices; i++)
		{
			_vecinos.add(new HashSet<Integer>());
			for(int j=0; j<vertices; j++)
				pesoAristas[i][j] = (i==j ? 0 : Double.POSITIVE_INFINITY);
		}
		
		menorArista = Double.POSITIVE_INFINITY;
		mayorArista = 0;
		pesoTotal = 0;
	}
	
	// Cantidad de v�rtices
	public int vertices()
	{
		return _vecinos.size();
	}
	
	// Agregar una arista
	public void agregarArista(int i, int j, double peso)
	{
		verificarIndicesValidos(i, j);
		verificarAristaInexistente(i, j);
		
		_vecinos.get(i).add(j);
		_vecinos.get(j).add(i);
		agregarAristaPeso(i,j,peso);
		actualizarMinMaxTot();
	}

	// Agrega peso de arista
	private void agregarAristaPeso(int i, int j, double peso)
	{
		this.pesoAristas[i][j] = peso;
		this.pesoAristas[j][i] = peso;
	}
	
	// Elimina una arista
	public void eliminarArista(int i, int j)
	{
		verificarIndicesValidos(i, j);
		verificarAristaExistente(i, j);
		
		_vecinos.get(i).remove(j);
		_vecinos.get(j).remove(i);
		eliminarAristaPeso(i,j);
		actualizarMinMaxTot();
	}
	
	//Elimina peso de la distancia entre vertices
	private void eliminarAristaPeso(int i, int j)
	{
		pesoAristas[i][j] = Double.POSITIVE_INFINITY;
		pesoAristas[j][i] = Double.POSITIVE_INFINITY;
	}
	
	// Consulta si existe una arista
	public boolean existeArista(int i, int j)
	{
		verificarIndicesValidos(i, j);
		return _vecinos.get(i).contains(j);
	}
	
	//Devuelve peso de la arista de los vertices recibidos
	public double getDistanciaArista(int i, int j)
	{
		verificarIndicesValidos(i,j);
		verificarPesosCoinciden(i,j);
		return pesoAristas[i][j];
	}

	//Devuelve si existe vertice
	public boolean existeVertice(int vertice)
	{
		return vertice < this.vertices() && vertice >= 0;
	}

	//Actualiza arista menor, mayor y peso total
	private void actualizarMinMaxTot()
	{
		calcularMenorArista();
		calcularMayorArista();
		calcularPesoTotal();
	}
	
	//Calcula la arista de menor peso
	private void calcularMenorArista()
	{
		this.menorArista = Double.POSITIVE_INFINITY;
		for(int i = 0; i < vertices()-1; i++)
		{
			for(int j = i+1; j < vertices(); j++)
			{
				double distancia = getDistanciaArista(i,j);
				if(distancia != 0 && distancia != Double.POSITIVE_INFINITY)
				{
					if(distancia < this.menorArista)
					{
						this.menorArista = distancia;
						this.menorVerticeI = i;
						this.menorVerticeJ = j;
					}
				}
			}
		}
		ordenarVerticesMinMax();
	}
	
	//Calcula la arista de mayor peso
	private void calcularMayorArista()
	{
		this.mayorArista = 0;
		for(int i = 0; i < vertices()-1; i++)
		{
			for(int j = i+1; j < vertices(); j++)
			{
				double distancia = getDistanciaArista(i,j);
				if(distancia != 0 && distancia != Double.POSITIVE_INFINITY)
				{
					if(distancia > this.mayorArista)
					{
						this.mayorArista = distancia;
						this.mayorVerticeI = i;
						this.mayorVerticeJ = j;
					}
				}
			}
		}
		ordenarVerticesMinMax();
	}
	
	//Ordena los vertices de las aristas mayor y menor
	private void ordenarVerticesMinMax()
	{
		int aux;
		if(this.menorVerticeI > this.menorVerticeJ)
		{
			aux = this.menorVerticeJ;
			this.menorVerticeJ = this.menorVerticeI;
			this.menorVerticeI = aux;
		}
		if(this.mayorVerticeI > this.mayorVerticeJ)
		{
			aux = this.mayorVerticeJ;
			this.mayorVerticeJ = this.mayorVerticeI;
			this.mayorVerticeI = aux;
		}
	}
	
	//Calcula peso total de arbol
	private void calcularPesoTotal()
	{
		this.pesoTotal = 0;
		for(int i = 0; i < vertices()-1; i++)
		{
			for(int j = i+1; j < vertices(); j++)
			{
				double distancia = getDistanciaArista(i,j);
				if(distancia != 0 && distancia != Double.POSITIVE_INFINITY)
					this.pesoTotal+=distancia;
			}
		}
	}
	
	public double getMenorArista()
	{
		return this.menorArista;
	}
	
	public int getMenorVerticeJ()
	{
		return this.menorVerticeJ;
	}
	
	public int getMayorVerticeI()
	{
		return this.mayorVerticeI;
	}
	
	public int getMayorVerticeJ()
	{
		return this.mayorVerticeJ;
	}
	
	public int getMenorVerticeI()
	{
		return this.menorVerticeI;
	}
	
	public boolean verticesIMinMaxIguales()
	{
		return getMenorVerticeI() == getMayorVerticeI();
	}
	
	public boolean verticesJMinMaxIguales()
	{
		return getMenorVerticeJ() == getMayorVerticeJ();
	}
	
	public double getMayorArista()
	{
		return this.mayorArista;
	}
	
	public double getPesoTotal()
	{
		return this.pesoTotal;
	}
	
	//Devuelve arbol generador minimo de una instancia de grafo
	public Grafo agm()
	{
		Grafo agm = new Grafo(this.vertices());
		ArrayList<Integer> visitados = new ArrayList<Integer>();
		ArrayList<Integer> noVisitados = new ArrayList<Integer>();
		int verticeI = 0;
		int verticeJ = 0;
		double distancia = Double.POSITIVE_INFINITY;
		
		visitados.add(0);
		for(int i = 1; i < this.vertices(); i++)
			noVisitados.add(i);
		
		while(!noVisitados.isEmpty())
		{
			for(Integer i : visitados)
			{
				for(Integer j : noVisitados)
				{
					if(getDistanciaArista(i,j) < distancia && !i.equals(j))
					{
						verticeI = i;
						verticeJ = j;
						distancia = getDistanciaArista(i,j);
					}
				}
			}
			agm.agregarArista(verticeI,verticeJ,distancia);
			visitados.add(verticeJ);
			noVisitados.remove(noVisitados.indexOf(verticeJ));
			
			verticeI = 0;
			verticeJ = 0;
			distancia = Double.POSITIVE_INFINITY;
		}
		return agm;
	}
	
	//Verifica que el peso ida y vuelta entre aristas coincide
	private void verificarPesosCoinciden(int i, int j)
	{
		if(pesoAristas[i][j] != pesoAristas[j][i])
		{
			throw new IllegalArgumentException("Los pesos no coinciden entre si");
		}
	}
	
	//Lanza excepcion si se intenta crear grafo con cantidad de vertices negativa
	private void verificarCantidadVerticesValido(int vertices)
	{
		if(vertices < 0)
			throw new IllegalArgumentException("No se puede crear grafo con cantidad negativa de vertices");
	}

	// Lanza excepciones si i no es un vertice valido
	private void verificarVertice(int i)
	{
		if( i < 0 || i >= vertices() )
			throw new IllegalArgumentException("El vertice " + i + " no existe!");
	}
	
	// Lanza excepciones si (i,j) no puede ser una arista nueva
	private void verificarIndicesValidos(int i, int j)
	{
		verificarVertice(i);
		verificarVertice(j);
		
		if( i == j )
			throw new IllegalArgumentException("�No se pueden agregar loops! V�rtice = " + i);
	}
	
	// Lanza una excepci�n si la arista existe
	private void verificarAristaInexistente(int i, int j)
	{
		if( existeArista(i, j) == true )
			throw new IllegalArgumentException("�Se intent� agregar una arista existente! (i,j) = (" + i + "," + j +")");
	}
	
	// Lanza una excepci�n si la arista no existe
	private void verificarAristaExistente(int i, int j)
	{
		if( existeArista(i, j) == false )
			throw new IllegalArgumentException("�Se intent� eliminar una arista inexistente! (i,j) = (" + i + "," + j +")");
	}
}