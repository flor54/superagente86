package logic;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;
import org.openstreetmap.gui.jmapviewer.Coordinate;

public class AgenteTest {

	Coordinate ubicacionRandom;	
	Agente agenteVacio;
	Agente agenteNormal;
	
	@Before
	public void inicializarTest()
	{
		Coordinate ubicacionRandom = ubicacionRandom();
		
		agenteVacio = new Agente();
		agenteNormal = new Agente("Fulano", ubicacionRandom);
	}

	@Test(expected = RuntimeException.class)
	public void agenteVacioSinNombreTest()
	{
		agenteVacio.getNombreClave();
	}
	
	@Test(expected = RuntimeException.class)
	public void agenteVacioSinUbicacionTest()
	{
		agenteVacio.getUbicacion();
	}
	
	@Test
	public void agenteNormalConNombreTest()
	{
		String nombre = agenteNormal.getNombreClave();
		assertNotNull(nombre);
	}
	
	@Test
	public void agenteNormalConUbicacionTest()
	{
		Coordinate ubicacion = agenteNormal.getUbicacion();
		assertNotNull(ubicacion);
	}

	private Coordinate ubicacionRandom()
	{
		Random doubleRandom = new Random();
		Random intRandom = new Random();
		double lon = (double) (intRandom.nextInt(361) + doubleRandom.nextDouble())-180;
		double lat = (double) (intRandom.nextInt(181) + doubleRandom.nextDouble())-90;
		return new Coordinate(lat, lon);
	}
}
